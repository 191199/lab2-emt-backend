package com.example.lab2.mapper;

import com.example.lab2.model.Author;
import com.example.lab2.model.Country;
import com.example.lab2.model.dto.in.AuthorIn;
import com.example.lab2.model.dto.out.AuthorOut;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;


@Mapper
public interface AuthorMapper {
    @Mapping(source = "authorIn.name", target = "name")
    @Mapping(source = "authorIn.surname", target = "surname")
    @Mapping(source = "authorIn.country", target = "country")
    Author updateAuthor(AuthorIn authorIn, @MappingTarget Author author);

    Author toEntity(AuthorIn authorIn);

    default Country fromIdToCountry(Long id) {
        return Country.builder().id(id).build();
    }

    AuthorOut toDto(Author author);
}
