package com.example.lab2.mapper;

import com.example.lab2.model.Country;
import com.example.lab2.model.dto.in.CountryIn;
import com.example.lab2.model.dto.out.CountryOut;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;


@Mapper
public interface CountryMapper {

    @Mapping(source = "countryIn.name", target = "name")
    @Mapping(source = "countryIn.continent", target = "continent")
    Country updateCountry(CountryIn countryIn, @MappingTarget Country country);

    Country toEntity(CountryIn countryIn);

    CountryOut toDto(Country country);
}
