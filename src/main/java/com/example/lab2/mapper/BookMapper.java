package com.example.lab2.mapper;

import com.example.lab2.model.Author;
import com.example.lab2.model.Book;
import com.example.lab2.model.dto.in.BookIn;
import com.example.lab2.model.dto.out.BookOut;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;


@Mapper
public interface BookMapper {
  @Mapping(source = "bookIn.name", target = "name")
  @Mapping(source = "bookIn.category", target = "category")
  @Mapping(source = "bookIn.author", target = "author")
  @Mapping(source = "bookIn.availableCopies", target = "availableCopies")
  Book updateBook(BookIn bookIn, @MappingTarget Book book);

  default Author fromIdToAuthor(Long id) {
    return Author.builder().id(id).build();
  }

  Book toEntity(BookIn bookIn);

  BookOut toDto(Book book);
}
