package com.example.lab2.service.impl;

import com.example.lab2.mapper.CountryMapper;
import com.example.lab2.model.Country;
import com.example.lab2.model.dto.in.CountryIn;
import com.example.lab2.model.dto.out.CountryOut;
import com.example.lab2.repository.jpa.CountryRepository;
import com.example.lab2.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService {
    private final CountryRepository countryRepository;
    private final CountryMapper countryMapper;

    @Override
    public CountryOut save(CountryIn countryIn) {
        Country country = countryRepository.save(countryMapper.toEntity(countryIn));
        return countryMapper.toDto(country);
    }

    @Override
    public List<Country> findAll() {
        return countryRepository.findAll();
    }

    @Override
    public Optional<CountryOut> findById(Long id) {
        Country country = countryRepository.getById(id);
        return Optional.of(countryMapper.toDto(country));
    }

    @Override
    public Optional<CountryOut> edit(Long id, CountryIn countryIn) {
        Country countryToBeUpdated = countryRepository.getById(id);
        Country country = countryRepository.save(countryMapper.updateCountry(countryIn, countryToBeUpdated));
        return Optional.of(countryMapper.toDto(country));
    }

    @Override
    public void deleteById(Long id) {
        countryRepository.deleteById(id);
    }
}
