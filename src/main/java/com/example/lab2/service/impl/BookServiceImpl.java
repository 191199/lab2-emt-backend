package com.example.lab2.service.impl;

import com.example.lab2.mapper.BookMapper;
import com.example.lab2.model.Book;
import com.example.lab2.model.dto.in.BookIn;
import com.example.lab2.model.dto.out.BookOut;
import com.example.lab2.model.enumeration.Category;
import com.example.lab2.model.exception.NoAvailableCopiesLeftException;
import com.example.lab2.repository.jpa.BookRepository;
import com.example.lab2.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final BookMapper bookMapper;

    @Override
    @Transactional
    public BookOut save(BookIn bookIn) {
        Book book = bookRepository.save(bookMapper.toEntity(bookIn));
        return bookMapper.toDto(book);
    }

    @Override
    public List<BookOut> findAll() {
        return bookRepository.findAll()
                .stream()
                .map(bookMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<BookOut> findById(Long id) {
        Book book = bookRepository.getById(id);
        return Optional.of(bookMapper.toDto(book));
    }

    @Override
    @Transactional
    public Optional<BookOut> edit(Long id, BookIn bookIn) {
        Book bookToBeUpdated = bookRepository.getById(id);
        Book book = bookRepository.save(bookMapper.updateBook(bookIn, bookToBeUpdated));
        return Optional.of(bookMapper.toDto(book));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void markAsTaken(Long id) {
        Book book = bookRepository.getById(id);
        if (book.getAvailableCopies() > 0) {
            book.setAvailableCopies(book.getAvailableCopies() - 1);
            bookRepository.save(book);
        } else {
            throw new NoAvailableCopiesLeftException();
        }
    }

    @Override
    public Page<Book> findAllWithPagination(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    @Override
    public List<Category> getAllCategories() {
        return List.of(Category.values());
    }
}
