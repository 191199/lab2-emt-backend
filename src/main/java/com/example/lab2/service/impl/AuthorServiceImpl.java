package com.example.lab2.service.impl;

import com.example.lab2.mapper.AuthorMapper;
import com.example.lab2.model.Author;
import com.example.lab2.model.dto.in.AuthorIn;
import com.example.lab2.model.dto.out.AuthorOut;
import com.example.lab2.repository.jpa.AuthorRepository;
import com.example.lab2.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    @Override
    public AuthorOut save(AuthorIn authorIn) {
        Author author =  authorRepository.save(authorMapper.toEntity(authorIn));
        return authorMapper.toDto(author);
    }

    @Override
    public Optional<AuthorOut> edit(Long id, AuthorIn authorIn) {
        Author authorToBeUpdated = authorRepository.getById(id);
        Author author = authorRepository.save(authorMapper.updateAuthor(authorIn, authorToBeUpdated));
        return Optional.of(authorMapper.toDto(author));
    }

    @Override
    public void deleteById(Long id) {
        authorRepository.deleteById(id);
    }

    @Override
    public List<Author> findAll() {
        return authorRepository.findAll();
    }

    @Override
    public Optional<AuthorOut> findById(Long id) {
        Author author = authorRepository.getById(id);
        return Optional.of(authorMapper.toDto(author));
    }
}
