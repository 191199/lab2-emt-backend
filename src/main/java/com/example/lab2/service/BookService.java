package com.example.lab2.service;

import com.example.lab2.model.Book;
import com.example.lab2.model.dto.in.BookIn;
import com.example.lab2.model.dto.out.BookOut;
import com.example.lab2.model.enumeration.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface BookService {
    BookOut save(BookIn bookIn);

    List<BookOut> findAll();

    Optional<BookOut> findById(Long id);

    Optional<BookOut> edit(Long id, BookIn bookIn);

    void deleteById(Long id);

    void markAsTaken(Long id);

    Page<Book> findAllWithPagination(Pageable pageable);

    List<Category> getAllCategories();
}
