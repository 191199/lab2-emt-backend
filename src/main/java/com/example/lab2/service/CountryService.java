package com.example.lab2.service;

import com.example.lab2.model.Country;
import com.example.lab2.model.dto.in.CountryIn;
import com.example.lab2.model.dto.out.CountryOut;

import java.util.List;
import java.util.Optional;

public interface CountryService {
    CountryOut save(CountryIn countryIn);

    List<Country> findAll();

    Optional<CountryOut> findById(Long id);

    Optional<CountryOut> edit(Long id, CountryIn countryIn);

    void deleteById(Long id);
}
