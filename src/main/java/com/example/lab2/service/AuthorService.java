package com.example.lab2.service;

import com.example.lab2.model.Author;
import com.example.lab2.model.dto.in.AuthorIn;
import com.example.lab2.model.dto.out.AuthorOut;

import java.util.List;
import java.util.Optional;

public interface AuthorService {
    AuthorOut save(AuthorIn authorIn);

    Optional<AuthorOut> edit(Long id, AuthorIn authorIn);

    void deleteById(Long id);

    List<Author> findAll();

    Optional<AuthorOut> findById(Long id);
}
