package com.example.lab2.web;

import com.example.lab2.model.Author;
import com.example.lab2.model.Book;
import com.example.lab2.model.dto.in.BookIn;
import com.example.lab2.model.dto.out.BookOut;
import com.example.lab2.model.enumeration.Category;
import com.example.lab2.service.AuthorService;
import com.example.lab2.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequiredArgsConstructor
@RequestMapping("/api/books")
public class BookRestController {

  private final BookService bookService;
  private final AuthorService authorService;

  @GetMapping("/")
  public List<BookOut> findAll() {
    return bookService.findAll();
  }

  @GetMapping
  public List<Book> findAllWithPagination(Pageable pageable) {
    return bookService.findAllWithPagination(pageable).getContent();
  }

  @GetMapping("/{id}")
  public ResponseEntity<BookOut> findById(@PathVariable Long id) {
    return bookService.findById(id)
        .map(b -> ResponseEntity.ok().body(b))
        .orElseGet(() -> ResponseEntity.badRequest().build());
  }

  @PostMapping("/add")
  public BookOut save(@RequestBody BookIn bookIn) {
    return bookService.save(bookIn);
  }

  @PutMapping("/edit/{id}")
  public ResponseEntity<BookOut> edit(@PathVariable Long id, @RequestBody BookIn bookIn) {
    return bookService.edit(id, bookIn)
        .map(b -> ResponseEntity.ok().body(b))
        .orElseGet(() -> ResponseEntity.badRequest().build());
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity deleteById(@PathVariable Long id) {
    bookService.deleteById(id);
    return ResponseEntity.ok().build();
  }

  @PutMapping("/rent/{id}")
  public void markAsTaken(@PathVariable Long id) {
    bookService.markAsTaken(id);
  }

  @GetMapping("/categories")
  public List<Category> getAllCategories() {
    return bookService.getAllCategories();
  }

  @GetMapping("/authors")
  public List<Author> getAllAuthors() {
    return authorService.findAll();
  }
}
