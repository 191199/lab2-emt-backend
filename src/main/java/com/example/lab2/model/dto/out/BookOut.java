package com.example.lab2.model.dto.out;

import com.example.lab2.model.Author;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class BookOut {
    private Long id;
    private String name;
    private String category;
    private Author author;
    private Integer availableCopies;
}
