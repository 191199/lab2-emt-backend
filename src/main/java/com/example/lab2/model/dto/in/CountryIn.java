package com.example.lab2.model.dto.in;

import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
public class CountryIn {
    private String name;
    private String continent;
}
