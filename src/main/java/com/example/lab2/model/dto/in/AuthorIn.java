package com.example.lab2.model.dto.in;

import com.example.lab2.model.Country;
import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
public class AuthorIn {
    private String name;
    private String surname;
    private Long country;
}
