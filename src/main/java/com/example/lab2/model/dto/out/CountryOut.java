package com.example.lab2.model.dto.out;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class CountryOut {
    private Long id;
    private String name;
    private String continent;
}
