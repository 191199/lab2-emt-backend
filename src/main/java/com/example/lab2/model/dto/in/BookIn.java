package com.example.lab2.model.dto.in;

import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
public class BookIn {
    private String name;
    private String category;
    private Long author;
    private Integer availableCopies;
}
