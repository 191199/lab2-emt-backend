package com.example.lab2.model.dto.out;

import com.example.lab2.model.Country;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class AuthorOut {
    private String name;
    private String surname;
    private Country countries;
}