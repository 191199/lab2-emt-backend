package com.example.lab2.model.enumeration;

public enum Category {
    NOVEL, THRILER, HISTORY, FANTASY, BIOGRAPHY, CLASSICS, DRAMA
}
