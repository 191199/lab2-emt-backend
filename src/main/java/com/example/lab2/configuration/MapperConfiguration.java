package com.example.lab2.configuration;

import com.example.lab2.mapper.AuthorMapperImpl;
import com.example.lab2.mapper.BookMapperImpl;
import com.example.lab2.mapper.CountryMapperImpl;
import org.springframework.context.annotation.Import;

@Import({
        AuthorMapperImpl.class,
        BookMapperImpl.class,
        CountryMapperImpl.class
})
class MapperConfiguration {
}
