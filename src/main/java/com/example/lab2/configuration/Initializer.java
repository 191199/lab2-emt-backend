package com.example.lab2.configuration;

import com.example.lab2.model.Author;
import com.example.lab2.model.Country;
import com.example.lab2.repository.jpa.AuthorRepository;
import com.example.lab2.repository.jpa.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class Initializer {
  private final AuthorRepository authorRepository;
  private final CountryRepository countryRepository;

  @PostConstruct
  void init(){
    Country c=new Country("Macedonia", "Europe");
    Author a=new Author("Bob","Jackson",c);
    countryRepository.save(c);
    authorRepository.save(a);

    Country c1=new Country("England", "Europe");
    Author a1=new Author("George","Martin",c);
    countryRepository.save(c1);
    authorRepository.save(a1);

    Country c2=new Country("USA", "America");
    Author a2=new Author("Jack","Johnson",c);
    countryRepository.save(c2);
    authorRepository.save(a2);
  }
}
